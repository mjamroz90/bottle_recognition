import sys
import os.path as op

import numpy as np
import cv2

import config
from utils.logger import log

sys.path.insert(0, op.join(config.RESNET_CAFFE_ROOT_DIR, 'python'))
import caffe


@log
class ResNet50Classifier(object):
    def __init__(self):

        def load_mean():
            blob = caffe.proto.caffe_pb2.BlobProto()
            data = open(config.RECOG_CAFFE_MEAN, 'rb').read()
            blob.ParseFromString(data)
            arr = np.array(caffe.io.blobproto_to_array(blob))
            mean = arr[0]
            return mean

        self.caffe_trained_model = config.RECOG_CAFFE_MODEL
        self.test_net_prototxt = config.RECOG_CAFFE_NET
        self.net = caffe.Net(str(self.test_net_prototxt), str(self.caffe_trained_model), caffe.TEST)
        self.transformer = caffe.io.Transformer({'data': self.net.blobs['data'].data.shape})
        self.transformer.set_transpose('data', (2, 0, 1))

        try:
            mean_arr = load_mean()
            if mean_arr.shape[1:] != self.image_size():
                mean_arr = cv2.resize(np.swapaxes(mean_arr, 0, 2).swapaxes(0, 1), self.image_size()[::-1],
                                      interpolation=cv2.INTER_NEAREST)
                mean_arr = np.swapaxes(mean_arr, 0, 2).swapaxes(1, 2)
        except (KeyError, IOError):
            mean_arr = np.array([104, 117, 123], dtype=np.float32)
        self.transformer.set_mean('data', mean_arr)
        self.logger.info("Initialized ResNet50 classifier - image size: %s, batch size: %d, mean shape: %s" % (
            str(self.image_size()), self.net_batch_size(), str(mean_arr.shape)))

        caffe.set_mode_gpu()
        caffe.set_device(config.GPU_ID)

    def preprocess_image(self, image):
        target_size = self.image_size()
        if image.shape[0] <= target_size[0] or image.shape[1] <= target_size[1]:
            image = cv2.resize(image, target_size)
        transformed_image = self.transformer.preprocess('data', image)
        return transformed_image

    def get_score_vector(self, image):
        img_processed = self.preprocess_image(image)
        self.net.forward_all(data=np.array([img_processed]))
        probs_vector = self.net.blobs['prob'].data[0, :]
        return probs_vector

    def get_batch_scores(self, images, batch_size=config.RECOG_NET_BATCH_SIZE):
        probabilities = []
        for i in xrange(0, len(images), batch_size):
            images_batch = images[i: i + batch_size]
            batch_shape = (len(images_batch),) + tuple(self.transformer.inputs['data'][1:])
            self.net.blobs['data'].reshape(*batch_shape)

            for index, image in enumerate(images_batch):
                image_data = self.preprocess_image(image)
                self.net.blobs['data'].data[index] = image_data

            self.net.forward()
            batch_probabilities = self.net.blobs['prob'].data
            probabilities.append(batch_probabilities.copy())

        probabilities = np.vstack(probabilities)
        return probabilities

    def image_size(self):
        return self.net.blobs['data'].data.shape[2:]

    def net_batch_size(self):
        return self.net.blobs['data'].data.shape[0]

    def __del__(self):
        del self.net
