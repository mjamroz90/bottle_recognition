import sys
import os.path as op

import numpy as np
import cv2

import config
from utils.logger import log

sys.path.insert(0, op.join(config.SSD_CAFFE_MODEL, 'python'))
import caffe


@log
class SSDDetector(object):
    def __init__(self, ssd_thresh):
        self.thresh = ssd_thresh
        self.caffe_trained_model = config.SSD_CAFFE_MODEL
        self.test_net_prototxt = config.SSD_CAFFE_NET
        self.net = self._create_caffe_net()
        self.transformer = self._create_transformer()
        self.positive_label = 1

        caffe.set_mode_gpu()
        caffe.set_device(config.GPU_ID)

    def _create_caffe_net(self):
        return caffe.Net(str(self.test_net_prototxt), str(self.caffe_trained_model), caffe.TEST)

    def _create_transformer(self):
        transformer = caffe.io.Transformer({'data': self.net.blobs['data'].data.shape})
        transformer.set_transpose('data', (2, 0, 1))
        transformer.set_mean('data', np.array([104, 117, 123]))  # mean pixel
        return transformer

    def get_detections(self, img):
        transformed_img = self.transformer.preprocess('data', img)
        self.net.forward_all(data=np.array([transformed_img]))
        detections = self.net.blobs['detection_out'].data
        img_detections = detections[0, 0, :, 1:]
        img_detections = [[max(0, x_min), max(0, y_min), min(img.shape[1], x_max), min(img.shape[0], y_max), conf]
                          for l, conf, x_min, y_min, x_max, y_max in img_detections if int(l) == self.positive_label]
        best_img_detections = self.filter_detections(img, img_detections)
        return best_img_detections

    def filter_detections(self, image, detections):
        image_height, image_width = image.shape[:2]
        results = [self.detection_to_result(conf, x_min, y_min, x_max, y_max, image.shape[:2])
                   for x_min, y_min, x_max, y_max, conf in detections
                   if conf >= self.thresh]
        results = [[x_min, y_min, x_max, y_max, conf]
                   for x_min, y_min, x_max, y_max, conf in results
                   if (x_min > 0 and x_max <= image_width) and
                   (y_min > 0 and y_max <= image_height)]
        results = [[x_min, y_min, x_max, y_max, conf]
                   for x_min, y_min, x_max, y_max, conf in results
                   if (x_max - x_min) < image_height * 0.3 and (y_max - y_min) < image_width * 0.3]
        results.sort(key=lambda x: -x[-1])
        return results

    @staticmethod
    def detection_to_result(conf, x_min, y_min, x_max, y_max, (image_height, image_width)):
        conf = float(conf)
        x_min = int(round(x_min * image_width))
        y_min = int(round(y_min * image_height))
        x_max = int(round(x_max * image_width))
        y_max = int(round(y_max * image_height))
        result = [x_min, y_min, x_max, y_max, conf]
        return result
