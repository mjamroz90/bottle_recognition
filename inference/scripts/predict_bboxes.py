import argparse

import cv2

from inference.models.ssd import SSDDetector
from utils import fs
from utils import vis_utils


def detect_bottles(img_arr, thresh):
    detector = SSDDetector(thresh)
    detections_list = detector.get_detections(img_arr)
    return [{'xmin': xmin, 'ymin': ymin, 'xmax': xmax, 'ymax': ymax, 'conf': conf}
            for xmin, ymin, xmax, ymax, conf in detections_list]


def visualize_bboxes(img_arr, detections):
    img_arr1 = img_arr.copy()
    for bbox in detections:
        xmin, ymin, xmax, ymax, conf = bbox['xmin'], bbox['ymin'], bbox['xmax'], bbox['ymax'], bbox['conf']
        vis_utils.draw_bbox(img_arr1, (xmin, ymin, xmax, ymax), color=[255, 0, 0])
        cv2.putText(img_arr1, str(conf), (xmin, ymin), cv2.FONT_HERSHEY_PLAIN, 1, [0, 0, 255], 2)
    return img_arr1


def main():
    args = parse_args()
    img = cv2.imread(args.in_img_path)
    detections = detect_bottles(img, args.ssd_thresh)
    fs.write_json(detections, args.out_detections_json_file)
    img_with_vis = visualize_bboxes(img, detections)
    cv2.imwrite(args.out_detections_vis_file, img_with_vis)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('in_img_path')
    parser.add_argument('out_detections_json_file')
    parser.add_argument('out_detections_vis_file')
    parser.add_argument('--ssd_thresh', type=float, default=0.6)
    return parser.parse_args()


if __name__ == '__main__':
    main()
