import argparse

import cv2
import numpy as np

from inference.models.resnet import ResNet50Classifier
from utils import fs
from utils.logger import log
from utils.vis_utils import generate_colors_palette, draw_bbox


def extract_patches(detections, img):
    patches = []
    for bbox in detections:
        xmin, ymin, xmax, ymax = bbox['xmin'], bbox['ymin'], bbox['xmax'], bbox['ymax']
        patches.append(img[ymin: ymax, xmin: xmax, :])
    return patches


@log
def classify_patches(patches):
    classifier = ResNet50Classifier()
    classify_patches.logger.info("Starting %d patches classification" % len(patches))
    patches_probs = classifier.get_batch_scores(patches)
    classify_patches.logger.info("Classified all %d patches" % len(patches))
    patches_classes = np.argmax(patches_probs, axis=1).astype(np.uint8)
    return patches_classes


def visualize_recognized_bottles(img, detections, patches_classes, classes_num):
    colors_palette = generate_colors_palette(classes_num)
    img1 = img.copy()
    for bbox, bbox_class in zip(detections, patches_classes):
        xmin, ymin, xmax, ymax = bbox['xmin'], bbox['ymin'], bbox['xmax'], bbox['ymax']
        color = colors_palette[bbox_class]
        draw_bbox(img1, (xmin, ymin, xmax, ymax), color=color)
    return img1


def write_recognition_results(patches_classes, out_file, labels_mapping):
    fs.write_json([labels_mapping[str(pc)] for pc in patches_classes], out_file)


def main():
    args = parse_args()
    detections = fs.read_json(args.detections_json_file)
    labels_mapping = fs.read_json(args.labels_mapping_file)
    img = cv2.imread(args.in_img_path)
    patches = extract_patches(detections, img)
    patches_classes = classify_patches(patches)
    vis_img = visualize_recognized_bottles(img, detections, patches_classes, len(labels_mapping))
    write_recognition_results(patches_classes, args.out_recognition_json, labels_mapping)
    cv2.imwrite(args.out_vis_path, vis_img)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('in_img_path')
    parser.add_argument('detections_json_file')
    parser.add_argument('out_recognition_json')
    parser.add_argument('labels_mapping_file')
    parser.add_argument('out_vis_path')
    return parser.parse_args()


if __name__ == '__main__':
    main()
