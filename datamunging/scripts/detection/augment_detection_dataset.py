import argparse
import os.path as op
import cv2

import config
from utils import img_ops
from utils import fs
from utils import bbox_utils
from utils.logger import log
from utils import vis_utils


AUGMENTING_FUNCTIONS = [('flip_x', img_ops.flip_x, bbox_utils.flip_bbox_x),
                        ('flip_y', img_ops.flip_y, bbox_utils.flip_bbox_y),
                        ('flip_xy', img_ops.flip_xy, bbox_utils.flip_bbox_xy)]


@log
def augment_dataset(img_paths_with_annotations, out_images_dir):
    img_paths_annotations_mapping = {}
    for i, (img_path, annotations_path) in enumerate(img_paths_with_annotations):
        coords_list = fs.parse_annotations_file(annotations_path)
        img = cv2.imread(img_path)
        img_h, img_w = img.shape[:2]
        bboxes_list = [bbox_utils.relative_coords_to_bbox(c, img_h, img_w) for c in coords_list]
        img_paths_annotations_mapping[img_path] = bboxes_list_to_hash_list(bboxes_list)
        cv2.imwrite(op.join(out_images_dir, op.basename(img_path)), img)
        for suffix, aug_img_func, aug_bbox_func in AUGMENTING_FUNCTIONS:
            aug_img = aug_img_func(img)
            aug_bboxes_list = bboxes_list_to_hash_list([aug_bbox_func(b, img_h, img_w) for b in bboxes_list])
            out_aug_img_path = op.abspath(op.join(out_images_dir, add_suffix_to_path(img_path, suffix)))
            img_paths_annotations_mapping[out_aug_img_path] = aug_bboxes_list

            cv2.imwrite(out_aug_img_path, aug_img)

        augment_dataset.logger.info("Processed %d/%d images" % (i, len(img_paths_with_annotations)))
    return img_paths_annotations_mapping


@log
def visualize_annotations(img_paths_annotations_mapping, out_dir):
    visualize_annotations.logger.info("Visualizing results ...")
    for img_path, img_annotations in img_paths_annotations_mapping.iteritems():
        img = cv2.imread(img_path)
        for bbox_hash in img_annotations:
            xmin, ymin, xmax, ymax = bbox_hash['xmin'], bbox_hash['ymin'], bbox_hash['xmax'], bbox_hash['ymax']
            vis_utils.draw_bbox(img, (xmin, ymin, xmax, ymax), color=[255, 0, 0])
        cv2.imwrite(op.join(out_dir, op.basename(img_path)), img)


def add_suffix_to_path(img_path, suffix):
    img_name = op.basename(img_path)
    prefix, ext = op.splitext(img_name)
    aug_img_path = "%s_%s%s" % (prefix, suffix, ext)
    return aug_img_path


def bboxes_list_to_hash_list(bboxes_list):
    return [{'xmin': xmin, 'ymin': ymin, 'xmax': xmax, 'ymax': ymax} for xmin, ymin, xmax, ymax in bboxes_list]


def main():
    args = parse_args()
    fs.create_dir_if_not_exists(args.out_images_dir)
    imgs_paths_with_annotations = fs.list_imgs_with_annotations(op.join(config.BOTTLE_DS_ROOT_DIR, 'Detection'))
    img_paths_annotations_mapping = augment_dataset(imgs_paths_with_annotations, args.out_images_dir)
    fs.write_json(img_paths_annotations_mapping, args.out_annotations_json_file)
    if args.out_visualizations_dir:
        fs.create_dir_if_not_exists(args.out_visualizations_dir)
        visualize_annotations(img_paths_annotations_mapping, args.out_visualizations_dir)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('out_images_dir')
    parser.add_argument('out_annotations_json_file')
    parser.add_argument('--out_visualizations_dir')
    return parser.parse_args()


if __name__ == '__main__':
    main()
