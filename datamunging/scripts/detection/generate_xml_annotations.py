import argparse
import os.path as op

from xml.etree import ElementTree
from xml.etree.ElementTree import SubElement, Element
from xml.dom import minidom

import cv2

from utils import fs
from utils.logger import log


@log
def generate_annotations(annotations_json, out_dir):
    for i, (img_path, img_bboxes) in enumerate(annotations_json.iteritems()):
        img_xml_doc = generate_single_xml(img_path, img_bboxes)
        xml_str = ElementTree.tostring(img_xml_doc, 'utf-8')
        xml_reparsed = minidom.parseString(xml_str)
        out_anno_path = op.join(out_dir, "%s.xml" % op.splitext(op.basename(img_path))[0])
        with open(out_anno_path, 'w') as f:
            f.write(xml_reparsed.toprettyxml())
        generate_annotations.logger.info("Generated %d/%d xml file" % (i, len(annotations_json)))


def generate_single_xml(img_path, img_bboxes):
    img = cv2.imread(img_path)
    img_h, img_w = img.shape[:2]
    root = Element("annotation")

    file_name_elem = SubElement(root, "filename")
    file_name_elem.text = op.basename(img_path)

    size_elem = SubElement(root, "size")
    SubElement(size_elem, "depth").text = str(3)
    SubElement(size_elem, "width").text = str(img_w)
    SubElement(size_elem, "height").text = str(img_h)
    SubElement(root, "segmented").text = str(0)

    for bbox in img_bboxes:
        obj_elem = SubElement(root, "object")
        SubElement(obj_elem, "name").text = "bottle"
        SubElement(obj_elem, "difficult").text = str(0)
        bnd_box_elem = SubElement(obj_elem, "bndbox")
        xmin, ymin, xmax, ymax = bbox['xmin'], bbox['ymin'], bbox['xmax'], bbox['ymax']

        SubElement(bnd_box_elem, "xmax").text = str(xmax)
        SubElement(bnd_box_elem, "xmin").text = str(xmin)
        SubElement(bnd_box_elem, "ymax").text = str(ymax)
        SubElement(bnd_box_elem, "ymin").text = str(ymin)

    return root


def main():
    args = parse_args()
    fs.create_dir_if_not_exists(args.out_annotations_dir)
    annotations_json = fs.read_json(args.json_annotations_file)
    generate_annotations(annotations_json, args.out_annotations_dir)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('json_annotations_file')
    parser.add_argument('out_annotations_dir')
    return parser.parse_args()


if __name__ == '__main__':
    main()

