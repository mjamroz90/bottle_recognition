import os.path as op
import argparse
import shutil

import xml.etree.ElementTree as ET

from utils import fs
from utils.logger import log
from augment_detection_dataset import visualize_annotations


@log
def find_bottle_images(voc_root_dir):
    annotations_dir = op.join(voc_root_dir, 'VOC2012/Annotations')
    images_dir = op.join(voc_root_dir, 'VOC2012/JPEGImages')
    xml_annotations_files = fs.list_files_from_dir(annotations_dir, ('xml',))
    bottle_paths_annotations = {}
    for i, xml_file in enumerate(xml_annotations_files):
        parse_result = parse_file_for_bottle(op.join(annotations_dir, xml_file))
        if parse_result is not None:
            img_name, bboxes = parse_result
            bottle_paths_annotations[op.abspath(op.join(images_dir, img_name))] = bboxes

        find_bottle_images.logger.info("Parsed %d/%d images" % (i, len(xml_annotations_files)))

    find_bottle_images.logger.info("Found %d bottle images" % len(bottle_paths_annotations))
    return bottle_paths_annotations


def parse_file_for_bottle(xml_file_path):
    root = ET.parse(xml_file_path).getroot()
    object_tags = root.findall('object')
    bottle_tags = [o for o in object_tags if o.find('name').text == 'bottle']
    if not bottle_tags:
        return None
    else:
        bottle_bboxes = []
        for bottle_tag in bottle_tags:
            bbox_elem = bottle_tag.find('bndbox')
            xmin, ymin = int(bbox_elem.find('xmin').text), int(bbox_elem.find('ymin').text)
            xmax, ymax = int(bbox_elem.find('xmax').text), int(bbox_elem.find('ymax').text)
            bottle_bboxes.append({'xmin': xmin, 'ymin': ymin, 'xmax': xmax, 'ymax': ymax})
        img_name = root.find('filename').text
        return img_name, bottle_bboxes


@log
def copy_bottle_images_with_annotations(bottle_images_hash, out_bottles_dir, out_json_file):
    copy_bottle_images_with_annotations.logger.info("Copying %d bottle images to %s" % (len(bottle_images_hash),
                                                                                        out_bottles_dir))
    for img_path in bottle_images_hash:
        shutil.copy(img_path, out_bottles_dir)
    copy_bottle_images_with_annotations.logger.info("Copying finished")
    fs.write_json(bottle_images_hash, out_json_file)


def main():
    args = parse_args()
    bottle_images_hash = find_bottle_images(args.voc_root_dir)
    if args.limit:
        bottle_images_hash = dict(bottle_images_hash.items()[:args.limit])
    fs.create_dir_if_not_exists(args.out_images_dir)
    copy_bottle_images_with_annotations(bottle_images_hash, args.out_images_dir, args.out_annotations_json_file)
    if args.out_visualizations_dir:
        fs.create_dir_if_not_exists(args.out_visualizations_dir)
        visualize_annotations(bottle_images_hash, args.out_visualizations_dir)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('voc_root_dir')
    parser.add_argument('out_images_dir')
    parser.add_argument('out_annotations_json_file')
    parser.add_argument('--out_visualizations_dir')
    parser.add_argument('--limit', type=int)
    return parser.parse_args()


if __name__ == '__main__':
    main()
