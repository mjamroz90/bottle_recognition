import argparse
import random
import os.path as op

from utils import fs
from utils.logger import log


@log
def split_bottle_paths_into_train_text(bottle_annotations):
    original_img_paths = [(p, op.basename(p)) for p in bottle_annotations if 'flip' not in op.basename(p)]
    augmented_img_paths = [(p, op.basename(p)) for p in bottle_annotations if 'flip' in op.basename(p)]
    train_size = int(0.9 * len(original_img_paths))
    random.shuffle(original_img_paths)
    train_original_paths = original_img_paths[:train_size]
    test_original_paths = original_img_paths[train_size:]
    train_paths, test_paths = [], []
    for aug_path, aug_name in augmented_img_paths:
        found = False
        for _, train_original_name in train_original_paths:
            if op.splitext(train_original_name)[0] in aug_name:
                train_paths.append(aug_path)
                found = True
                break
        if not found:
            for _, test_original_name in test_original_paths:
                if op.splitext(test_original_name)[0] in aug_name:
                    test_paths.append(aug_path)
                    break

    train_paths += [p[0] for p in train_original_paths]
    test_paths += [p[0] for p in test_original_paths]
    split_bottle_paths_into_train_text.logger.info("Split of bottle image paths: train = %d, test = %d" %
                                                   (len(train_paths), len(test_paths)))
    return train_paths, test_paths


@log
def save_set_to_txt_file(img_paths, annotations_xml_dir, out_txt_file):
    img_annotations_paths = [op.abspath(op.join(annotations_xml_dir, "%s.xml" % op.splitext(op.basename(p))[0]))
                             for p in img_paths]
    save_set_to_txt_file.logger.info("Saving set of %d images paths into %s" % (len(img_annotations_paths),
                                                                                out_txt_file))
    with open(out_txt_file, 'w') as f:
        for img_path, img_annotation_path in zip(img_paths, img_annotations_paths):
            f.write("%s %s\n" % (img_path[1:], img_annotation_path[1:]))


def main():
    args = parse_args()
    bottle_annotations = fs.read_json(args.bottles_annotations_json)
    train_bottle_paths, test_bottle_paths = split_bottle_paths_into_train_text(bottle_annotations)
    voc_annotations = fs.read_json(args.voc_annotations_json)
    train_bottle_paths.extend(voc_annotations.keys())

    fs.create_dir_if_not_exists(args.out_txt_dir)
    save_set_to_txt_file(train_bottle_paths, args.annotations_xml_dir, op.join(args.out_txt_dir, 'train.txt'))
    save_set_to_txt_file(test_bottle_paths, args.annotations_xml_dir, op.join(args.out_txt_dir, 'test.txt'))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('bottles_annotations_json')
    parser.add_argument('voc_annotations_json')
    parser.add_argument('annotations_xml_dir')
    parser.add_argument('out_txt_dir')
    return parser.parse_args()


if __name__ == '__main__':
    main()
