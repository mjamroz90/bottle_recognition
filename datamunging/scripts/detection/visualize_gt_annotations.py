import argparse
import os.path as op

import cv2

import config
from utils import fs
from utils import bbox_utils
from utils import vis_utils


def put_annotation_to_image(img, bbox):
    x_min, y_min, x_max, y_max = bbox
    cv2.rectangle(img, (x_min, y_min), (x_max, y_max), thickness=4, color=[255, 0, 0])


def visualize_annotations(img_paths_with_annotations, out_dir):
    for img_path, annotations_path in img_paths_with_annotations:
        img = cv2.imread(img_path)
        img_h, img_w = img.shape[:2]
        coords_list = fs.parse_annotations_file(annotations_path)
        bboxes_list = [bbox_utils.relative_coords_to_bbox(c, img_h, img_w) for c in coords_list]
        for bbox in bboxes_list:
            put_annotation_to_image(img, bbox)
            vis_utils.draw_bbox(img, bbox, color=[255, 0 ,0])
        out_path = op.join(out_dir, op.basename(img_path))
        cv2.imwrite(out_path, img)


def main():
    args = parse_args()
    fs.create_dir_if_not_exists(args.out_vis_dir)
    img_paths_with_annotations = fs.list_imgs_with_annotations(op.join(config.BOTTLE_DS_ROOT_DIR, 'Detection'))
    visualize_annotations(img_paths_with_annotations, args.out_vis_dir)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('out_vis_dir')
    return parser.parse_args()


if __name__ == '__main__':
    main()
