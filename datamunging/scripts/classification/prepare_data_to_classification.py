import os.path as op
import argparse
import random

import config
from utils import fs
from utils.logger import log


@log
def filter_images_with_labels(images_with_labels, examples_min):
    labels_count = {}
    for _, label in images_with_labels:
        if label not in labels_count:
            labels_count[label] = 1
        else:
            labels_count[label] += 1
    filtered_labels = set([l for l, l_count in labels_count.iteritems() if l_count >= examples_min])
    filter_images_with_labels.logger.info("Left %d labels having more than %d examples" %
                                          (len(filtered_labels), examples_min))
    return [(i, l) for i, l in images_with_labels if l in filtered_labels], filtered_labels


@log
def list_files_with_labels(recognition_dir, examples_min=None):
    label_dirs = fs.list_dirs(recognition_dir)
    labels_mapping = {}
    images_with_labels = []
    for i, label_dir in enumerate(label_dirs):
        label_images = fs.list_files_from_dir(op.join(recognition_dir, label_dir), ('jpg', 'png', 'jpeg'))
        label_images_paths = [op.abspath(op.join(recognition_dir, label_dir, p)) for p in label_images]
        for im_path in label_images_paths:
            images_with_labels.append((im_path, i))
        labels_mapping[i] = label_dir

    list_files_with_labels.logger.info("Listed all %d files, labels set: %d" % (len(images_with_labels),
                                                                                len(labels_mapping)))
    if examples_min is not None:
        images_with_labels, labels_left = filter_images_with_labels(images_with_labels, examples_min)
        labels_mapping = {i: labels_mapping[old_label] for i, old_label in enumerate(labels_left)}
        old_new_labels_mapping = {old_label: i for i, old_label in enumerate(labels_left)}
        images_with_labels = [(im_path, old_new_labels_mapping[l]) for im_path, l in images_with_labels]
        list_files_with_labels.logger.info("After filtering: Left %d files, labels set: %d" % (len(images_with_labels),
                                                                                               len(labels_mapping)))

    return images_with_labels, labels_mapping


@log
def split_into_train_test(images_with_labels, ratio):
    # want test set to be balanced - equal numbers of examples for each label
    labels_images_mapping = {}
    for im_path, l in images_with_labels:
        if l not in labels_images_mapping:
            labels_images_mapping[l] = [im_path]
        else:
            labels_images_mapping[l].append(im_path)

    min_examples_num = min([(k, len(v)) for k, v in labels_images_mapping.iteritems()], key=lambda x: x[1])[1]
    test_size_for_label = int((1.-ratio) * min_examples_num)
    test_ds, train_ds = [], []
    for l, l_examples in labels_images_mapping.iteritems():
        random.shuffle(l_examples)
        l_test_ds, l_train_ds = l_examples[:test_size_for_label], l_examples[test_size_for_label:]
        train_ds.extend([p, l] for p in l_train_ds)
        test_ds.extend([p, l] for p in l_test_ds)

    random.shuffle(train_ds)
    random.shuffle(test_ds)

    split_into_train_test.logger.info("Split into train/test: %d/%d" % (len(train_ds), len(test_ds)))
    return train_ds, test_ds


def save_ds_into_txt(images_with_labels, out_txt_file):
    with open(out_txt_file, 'w') as f:
        for im_path, label in images_with_labels:
            f.write("%s %d\n" % (im_path[1:], label))


def main():
    args = parse_args()
    examples_min = args.keep_labels_above if args.keep_labels_above else None
    images_with_labels, labels_mapping = list_files_with_labels(op.join(config.BOTTLE_DS_ROOT_DIR, 'Recognition'),
                                                                examples_min)
    fs.write_json(labels_mapping, args.out_labels_mapping_json_file)
    train_images_with_labels, test_images_with_labels = split_into_train_test(images_with_labels,
                                                                              args.train_whole_ratio)
    fs.create_dir_if_not_exists(args.out_txt_dir)
    save_ds_into_txt(train_images_with_labels, op.join(args.out_txt_dir, 'train.txt'))
    save_ds_into_txt(test_images_with_labels, op.join(args.out_txt_dir, 'test.txt'))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--train_whole_ratio', type=float, default=0.8)
    parser.add_argument('--keep_labels_above', type=int)
    parser.add_argument('out_txt_dir')
    parser.add_argument('out_labels_mapping_json_file')
    return parser.parse_args()


if __name__ == '__main__':
    main()
