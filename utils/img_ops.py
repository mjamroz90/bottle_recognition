import cv2


def flip_x(img):
    flipped_img = cv2.flip(img, 0)
    return flipped_img


def flip_y(img):
    flipped_img = cv2.flip(img, 1)
    return flipped_img


def flip_xy(img):
    flipped_img = cv2.flip(img, -1)
    return flipped_img
