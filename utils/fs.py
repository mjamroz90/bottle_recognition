import os
import os.path as op
import json


def create_dir_if_not_exists(dir_name):
    if not op.exists(dir_name):
        os.makedirs(dir_name)


def list_files_from_dir(dir_name, ext):
    return [p for p in os.listdir(dir_name) if p.endswith(ext)]


def list_dirs(dir_name):
    return [d for d in os.listdir(dir_name) if op.isdir(op.join(dir_name, d))]


def write_json(h, out_file):
    with open(out_file, 'w') as f:
        json.dump(h, f, indent=4)


def read_json(json_file):
    with open(json_file, 'r') as f:
        return json.load(f)


def list_imgs_with_annotations(detections_dir):
    imgs_paths = list_files_from_dir(detections_dir, ('jpg',))
    annotations_paths = [op.join(op.abspath(detections_dir), "%s.txt" % '.'.join(p.split('.')[:-1])) for p in imgs_paths]
    imgs_paths = [op.join(op.abspath(detections_dir), p) for p in imgs_paths]
    return zip(imgs_paths, annotations_paths)


def parse_annotations_file(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
    bboxes = [[float(c) for c in l.split(' ')[1:]] for l in lines]
    return bboxes

