def relative_coords_to_bbox(coords, img_h, img_w):
    mp_x, mp_y, w, h = coords
    x_min, y_min = mp_x - (w / 2.), mp_y - (h / 2.)
    x_min, y_min = int(x_min * img_w), int(y_min * img_h)
    x_max, y_max = x_min + int(img_w * w), y_min + int(img_h * h)
    return x_min, y_min, x_max, y_max


def flip_bbox_x(bbox, img_h, img_w):
    xmin, ymin, xmax, ymax = bbox
    xmin_f0, xmax_f0 = xmin, xmax
    ymin_f0, ymax_f0 = img_h - ymax, img_h - ymin
    return xmin_f0, ymin_f0, xmax_f0, ymax_f0


def flip_bbox_y(bbox, img_h, img_w):
    xmin, ymin, xmax, ymax = bbox
    ymin_f1, ymax_f1 = ymin, ymax
    xmax_f1, xmin_f1 = img_w - xmin, img_w - xmax
    return xmin_f1, ymin_f1, xmax_f1, ymax_f1


def flip_bbox_xy(bbox, img_h, img_w):
    xmin, ymin, xmax, ymax = bbox
    xmax_f_1, xmin_f_1 = img_w - xmin, img_w - xmax
    ymin_f_1, ymax_f_1 = img_h - ymax, img_h - ymin
    return xmin_f_1, ymin_f_1, xmax_f_1, ymax_f_1
