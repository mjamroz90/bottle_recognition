import cv2

import matplotlib
matplotlib.use("Agg")
from matplotlib import colors
from matplotlib import cm


def generate_colors_palette(colors_num):
    color_norm = colors.Normalize(vmin=0, vmax=colors_num-1)
    scalar_map = cm.ScalarMappable(norm=color_norm, cmap='Set1')

    def map_index_to_rgb_color(index):
        rgba_val = scalar_map.to_rgba(index)
        r, g, b, _ = rgba_val
        r, g, b = int(r * 255.), int(g * 255.), int(b * 255.)
        return b, g, r
    return [map_index_to_rgb_color(int(i)) for i in xrange(colors_num)]


def draw_bbox(img, (xmin, ymin, xmax, ymax), color):
    cv2.rectangle(img, (xmin, ymin), (xmax, ymax), color=color, thickness=4)
